// NodeJS modules
const https = require('https');

// npm modules
const mySQL = require('mysql');
const moment = require('moment');

// my other JS codes
const config = require('./realConfig.js'); // obviously, this isn't the commited config ;)

var p = 1;
var apiURL = '';
var keyword = 'ipfs';

var pageSize = 15;

// create database object and start the GET chain
const connection = mySQL.createConnection(config);

connection.connect();

getDataRecursive();

// functions


function getDataRecursive() {
    apiURL = `https://api.ipfs-search.com/v1/search?q=${keyword}&page=${p}`;

    https.get(apiURL, function (res) {
        let data = '';
        res.on('data', function (chunk) {
            data += chunk;
        }).on('end', function () {
            console.log(apiURL);

            // html escape
            let hits = [];
            let responseData = JSON.parse(data.replace(/<\/?[^>]+>/gi, ''));

            if (typeof responseData.hits !== 'undefined') {
                hits = responseData.hits;
            }

            hits.forEach(processResultItem);

            p++;

            if (Number(responseData.total) > pageSize * p || p < 100) {
                getDataRecursive() // repeat the entire query
            } else {
                console.log("End of results almost reached");
            }

            console.log(Number(responseData.total) + ">" + (pageSize * p));
        }).on('error', function (e) {
            console.log(e.message);
        })
    }).on('error', function (e) {
        console.log(e.message);
    });
}

//connection.end();




function processResultItem(result)
{

/*{
    "hash": "QmfGSKi6m4xokTwNwpcHAnghVQTXdv7j8VYCwbwfipDXk2",
    "title": "�<em>E</em>�D�<em>e</em>�L�9OcPb&amp;d�|�",
    "description": null,
    "type": "file",
    "size": 202,
    "last-seen": "2019-04-15T17:19:59Z",
    "score": 50.41694,
    "mimetype": "text/plain"
}*/

    let neededKeys = [
        "hash", 
        "title", 
        "description", 
        "type", 
        "size", 
        "first-seen", 
        "mimetype"
    ];

    let array = ["indexedOn"];
    let initValues = [];

    for (let k in result){
        if (result.hasOwnProperty(k) 
            && neededKeys.indexOf(k) > -1 
            && result[k] !== '' 
            && result[k] !== 'null'
        ) {

            // some keys should be rewritten
            if (k === "mimetype") {
                array.push("mime");
                initValues.push(result[k]);
            } else if (k === "first-seen") {
                array.push("firstSeen");
                initValues.push(moment(result[k]).format('YYYY-MM-DD HH:mm:ss'))
            } else {
                array.push(k);    
                initValues.push(result[k]);
            }

            
        }
    }

    let columns = array.join(",");
    let values = `now(),'${initValues.join("','")}'`;

    let query = `INSERT INTO ipfs__hash (${columns}) VALUES (${values}) ON DUPLICATE KEY UPDATE indexedOn = now();`

    connection.query(query, function(err, rows, fields) {
        if (err) {
            console.log('Error while performing Query:', err.message);
        }

    }).on('error', function(err) {
        // Handle error, an 'end' event will be emitted after this as well
        console.log(err.message);
    }).on('fields', function(fields) {
        // the field packets for the rows to follow
    }).on('result', function(row) {
        console.log("inserted " + result.hash)
    }).on('end', function() {
        // all rows have been received
    });
}